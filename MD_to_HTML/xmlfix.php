<?php
/**
 * Created by JetBrains PhpStorm.
 * User: edmund
 * Date: 7/25/17
 * Time: 3:26 PM
 * To change this template use File | Settings | File Templates.
 */


/**
 * @param $handle
 * @param $pattern string pattern to forward to
 * @param $echo boolean echo line to output
 */
function ff_to_text($handle, $pattern, $echo) {
    while (($line = fgets($handle)) !== false) {
        // process the line read.
        if ($echo)
            echo $line;

        if(preg_match($pattern, $line))
            break;
    }
}



function ff_to_toc($handle) {
    ff_to_text($handle, '/^\[TOC\]/', false);
}

function ff_to_mainmatter($handle) {
    ff_to_text($handle, '/^\{mainmatter\}/', true);
}

function remove_section_numbers($handle) {
    while (($line = fgets($handle)) !== false) {
        // process the line read.
/*
        $pattern = '/^(#+)(\s+)(\d+\.\d*)(\s*)(.+)(\s*)$/';
        $matches = null;

        // match ## 1.0 Scope
        if(preg_match($pattern, $line, $matches)) {
            echo $line;
            array_shift($matches);
            unset($matches[2]);
            $matches[0] = substr($matches[0], 0, strlen($matches[0]) -1);
            echo implode('', $matches);
            // print_r($matches);

//          break;
        }
*/

        $pattern = '/^(#+)(\s+)((\d+\.*)+\s*)(.+\s*)$/';
        $matches = null;

        // match ## 1.0.1 Scope
        if(preg_match($pattern, $line, $matches)) {
//            echo $line;
            array_shift($matches); // remove matched line
            unset($matches[2]);  // remove first whitespace match
            unset($matches[3]);
            $matches[0] = substr($matches[0], 0, strlen($matches[0]) -1);
            echo implode('', $matches);
//             print_r($matches);

//          break;
        } else
            echo $line;


    }


}

function test_appendix() {
        /*
         * ## Appendix A. Examples
         * ### A.1 Example request object


         */
        $oldpattern = '/^(#+)(\s+)((\d+\.*)+\s*)(.+\s*)$/';
        $pattern = '/^(#+)(\s+)(.*\s*)([A-Za-z]\.(\d+\.*)*\s*)(.+\s*)$/';
        $lines = [
            '## Appendix A. Examples',
            '### A.1 Example request object',
            '#### A.1.1 Example sub',
            '### blah blah',
            '## B2',
            'california is a state'
        ];
        $line = '## Appendix A. Examples';
        $matches = null;

        foreach ($lines as $line) {
            // match ## 1.0.1 Scope
            $matches = null;

            echo "\n-------------------------\n{$line}\n\n\n";
            if(preg_match($pattern, $line, $matches)) {
                array_shift($matches); // remove matched line
//                unset($matches[2]);  // remove matched section numbers and following spaces ((\d+\.*)+\s*)
//                unset($matches[3]);  // removes matched section numbers
                unset($matches[2]); // Appendix (words before section)
                unset($matches[3]); //
                unset($matches[4]); //
                $matches[0] = substr($matches[0], 0, strlen($matches[0]) -1);
                echo implode('', $matches) . "\n";
                print_r($matches);

//          break;
            } else
                echo $line;

        }



}


function remove_appendix_section_numbers($handle) {
    while (($line = fgets($handle)) !== false) {
        /*
         * ## Appendix A. Examples
         * ### A.1 Example request object
         */
        $pattern = '/^(#+)(\s+)(.*\s*)([A-Za-z]\.(\d+\.*)*\s*)(.+\s*)$/';
        $matches = null;

        // match ## 1.0.1 Scope
        if(preg_match($pattern, $line, $matches)) {
            array_shift($matches); // remove matched line
            unset($matches[2]); // Appendix (words before section)
            unset($matches[3]); // remove matched section numbers and following spaces ((\d+\.*)+\s*)
            unset($matches[4]); // removes matched section numbers
            $matches[0] = substr($matches[0], 0, strlen($matches[0]) -1);
            echo implode('', $matches) . "\n";
//             print_r($matches);

//          break;
        } else
            echo $line;


    }


}


function remove_section_numbers2($handle) {
    while (($line = fgets($handle)) !== false) {
        // process the line read.
        /*
                $pattern = '/^(#+)(\s+)(\d+\.\d*)(\s*)(.+)(\s*)$/';
                $matches = null;

                // match ## 1.0 Scope
                if(preg_match($pattern, $line, $matches)) {
                    echo $line;
                    array_shift($matches);
                    unset($matches[2]);
                    $matches[0] = substr($matches[0], 0, strlen($matches[0]) -1);
                    echo implode('', $matches);
                    // print_r($matches);

        //          break;
                }
        */

        $pattern = '/^(#+)(\s+)((\d+\.*)+\s*)(.+\s*)$/';
        $matches = null;

        // match ## 1.0.1 Scope
        if(preg_match($pattern, $line, $matches)) {
//            echo $line;
            array_shift($matches); // remove matched line
            unset($matches[2]);  // remove first whitespace match
            unset($matches[3]);
            $matches[0] = substr($matches[0], 0, strlen($matches[0]) -1);
            echo implode('', $matches);
//             print_r($matches);

//          break;
        } else if(preg_match('/\{backmatter\}/', $line, $match)) {
            echo $line;
            remove_appendix_section_numbers($handle);
        } else
            echo $line;


    }


}

function change_markdown($in_file) {
    $handle = fopen($in_file, "r");
    if ($handle) {
        ff_to_toc($handle);
        remove_section_numbers($handle);
        fclose($handle);
    } else {
        // error opening the file.
        printf('error openning file %s', $in_file);
    }
}

function change_markdown2($in_file) {
    $handle = fopen($in_file, "r");
    if ($handle) {
        ff_to_mainmatter($handle);
        remove_section_numbers2($handle);
        fclose($handle);
    } else {
        // error opening the file.
        printf('error openning file %s', $in_file);
    }
}

function remove_numbered_attrib($handle) {
    while (($line = fgets($handle)) !== false) {

        $pattern = '/numbered=\"true\" /';
        $matches = null;

        $count = 0;
        // match ## 1.0.1 Scope
        $changed = preg_replace($pattern, '', $line, -1, $count);
        if($changed && $count) {
            echo $changed;
        } else
            echo $line;
    }
}

function change_date($handle, $entity_file) {

    $foundVersion = false;
    $foundDate = false;
    $foundEntity = false;
    while (($line = fgets($handle)) !== false) {
        $output = $line;

        if(!$foundEntity) {
            $entityPattern = '/ENTITY pandocMiddle PUBLIC/';
            $entityMatches = null;

            if (preg_match($entityPattern, $line, $entityMatches)) {
                $output = sprintf("<!ENTITY pandocMiddle PUBLIC '' '%s'>\n", $entity_file);
                $foundEntity = true;
            }
        }

        if (!$foundVersion) {
            $versionPattern = '/private=\"(.+)-(\d+)\"/';
            $versionMatches = null;
            $versionCount = 0;

            if (preg_match($versionPattern, $line, $versionMatches)) {
                $newVersion = sprintf("private=\"%s-%02d\"", $versionMatches[1], $versionMatches[2] + 1);
                $versionChanged = preg_replace($versionPattern, $newVersion, $line, -1, $versionCount);
                if ($versionChanged && $versionCount) {
                    $output = $versionChanged;
                    $foundVersion = true;
                }
            }
        }

        if (!$foundDate) {
            $datePattern = '/date day=\".+\" month=\".+\" year=\".+\"/';
            $dateMatches = null;
            $dateCount = 0;
            $datecomponents = getdate();
            $curDate = sprintf("date day=\"%s\" month=\"%s\" year=\"%s\"", $datecomponents['mday'], $datecomponents['month'], $datecomponents['year']);
            $dateChanged = preg_replace($datePattern, $curDate, $line, -1, $dateCount);
            if ($dateChanged && $dateCount) {
                $output = $dateChanged;
                $foundDate = true;
            }
        }
        echo $output;
    }
}

function change_xml($in_file) {
    $handle = fopen($in_file, "r");
    if ($handle) {
        remove_numbered_attrib($handle);
        fclose($handle);
    } else {
        // error opening the file.
        printf('error openning file %s', $in_file);
    }
}

function change_template_date($in_file, $entity_file) {
    $handle = fopen($in_file, "r");
    if ($handle) {
        change_date($handle, $entity_file);
        fclose($handle);
    } else {
        // error opening the file.
        printf('error openning file %s', $in_file);
    }
}

if(isset($argv) && isset($argv[0]) && (basename($argv[0]) == basename(__FILE__))) {

    if($argc >= 2) {
        $executable = array_shift($argv);
        $command = array_shift($argv);
        $in_file = array_shift($argv);
        $entity_file = array_shift($argv);

        switch($command) {
            case 'md' :
                change_markdown($in_file);
                break;
            case 'md2' :
                change_markdown2($in_file);
                break;
            case 'xml' :
                change_xml($in_file);
                break;
            case 'dateVer' :
                change_template_date($in_file, $entity_file);
                break;
            case 'test' :
                test_appendix();
                break;
            default:
                print 'default';
                break;
        }
    } else {
        // No argument
        print 'No args';
    }
}