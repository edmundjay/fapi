#!/bin/sh

NUMFILES=0
FILEOPTION=0
SELECTEDFILE=''

printFiles() {

    echo "Select the file to convert :"
    echo ""
    for FILE in ../Financial_API_*.md
    do
        BASEFILE=`expr "$FILE" : '\.\./\(.*\)\.md'`
        TEMPLATE="${BASEFILE}_template.xml"
#        if [ -f $FILE ] && [ -f $TEMPLATE ]
        if [ -f $FILE ]
        then
            NUMFILES=`expr $NUMFILES + 1`
            echo "$NUMFILES) ${BASEFILE}.md"
            eval "FILE${NUMFILES}"="${BASEFILE}"

        fi
    done

    echo ""

    if [ $NUMFILES -eq 0 ]
    then
        echo "No template files found"
        exit 1
    fi
}

readOption() {
    while [ $FILEOPTION  -eq 0 ] && [ $FILEOPTION -le $1 ]
    do
        read FILEOPTION
        TEMPN=`expr "$FILEOPTION" : "^[0-9]*$"`
        if [ $TEMPN -eq 0 ]
        then
            eval FILEOPTION=0
            echo "Invalid file selection!!"
        elif [ $FILEOPTION -gt $1 ] || [ $FILEOPTION -eq 0 ]
        then
            echo "Invalid file selection!!"
            eval FILEOPTION=0
        else
            FILEOPTION=`expr "$FILEOPTION" : '0*\(.*\)'`
            eval "SELECTEDFILE"=$'FILE'${FILEOPTION}
        fi
    done

}

makeOutpuDir() {
    if [ ! -d output ]
    then
        mkdir -p output
        chmod 777 output
    fi
}

convertFile() {
    eval file=$1
    template_file="${file}_template.xml"
    if ! [  -f $template_file ]
    then
        template_file="Financial_API_Default_template.xml"
    fi
    entity_file="${file}.xml"

    php xmlfix.php md ../${file}.md > ${file}.md
    php xmlfix.php dateVer ./${template_file} ${entity_file} > ${file}_temp_template.xml
    ./pandoc2rfc/pandoc2rfc -X -t ${file}_temp_template.xml -x ./pandoc2rfc/transform.xsl ${file}.md
    mv -f draft.xml ${file}.xml
    tclsh ./xml2rfc/xml2rfc.tcl xml2html ./${file}.xml
    mv -f ${file}.html ${file}.xml ./output
    rm -f ${file}.md ${file}_temp_template.xml
}



printFiles
readOption $NUMFILES
echo $SELECTEDFILE
makeOutpuDir
convertFile $SELECTEDFILE
