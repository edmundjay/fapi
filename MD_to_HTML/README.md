# README #

Instructions for converting Markdown files to HTML and XML.
 

## Requirements ##

The conversion process depends on some specific versions of certain software, therefore in order to avoid complications
with installing these software, the conversion is run inside a Docker container. It will require an OS capable of 
running Docker. These instructions in this file apply to running Docker within a Linux OS. 

* Linux OS
* Lastest Docker software


## Docker Image ##

The Docker image used to run the conversion process can be built locally or a pre-built image can be retrieved from 
Docker Hub.

### Remote Docker Hub Image ###

A pre-built Docker image is available on Docker Hub. The image name is ***ewjay/fapibuild:1.0*** (Name and Location TBD)

### Local Docker Image (optional)  ###

The required Docker image can be built from the included Docker file.

> docker build -f fapi/MD_to_HTML/FapiDockerfile -t fapibuild .

The Docker image name will be ***fapibuild***.

### Markdown to XML/HTML Conversion ###

To convert the FAPI MarkDown spec files to XML/HTML, start the Docker image and run the conversion script.
This assumes that the FAPI respository has been cloned into a ***fapi*** subdirectory.
In a terminal, change to the directory which contains the ***fapi*** subdirectory, and issue the following commands :

> docker run -it --mount type=bind,source="$(pwd)"/fapi,target=/fapi ewjay/fapibuild:1.0

The Docker container is started and the container command prompt is shown. The ***fapi*** repository is mounted
inside the Docker container as ***/fapi*** and the files are shared between the container and host. Then issue the
following commands inside the container to perform the files conversion:

> cd /fapi/MD_to_HTML

> ./convert.sh

Converted files are saved under ***/fapi/MD_to_HTML/output*** in the container and are available on the host in the 
same relative path.


### Draft Version Numbers ###

In the ***fapi/MD_to_HTML*** subdirectory, the ***Financial_API_\*.template*** contains the current published draft 
version number for the respective specifications. During conversion, this draft version is incremented to create the new draft
version. When a new draft version is published, the corresponding template file's draft version number needs to be 
updated with the new published version number and committed back into source control, so that subsequent versions will have the correct version number.



